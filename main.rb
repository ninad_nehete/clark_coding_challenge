# frozen_string_literal: true

require 'require_all'
require 'sinatra'
require_all 'lib'

post '/calculate/referral_rewards' do
  file = params[:file][:tempfile].read
  options = {
    parser: Calculators::ReferralRewards::CommandParser,
    contract: Calculators::ReferralRewards::CommandContract,
    format: %i[date time user1 action user2]
  }

  tree = Calculators::ReferralRewards::TreeBuilder.new(file).execute(options)

  tree.points.to_json

rescue Exceptions::ApplicationError => e
  { errors: e.message }.to_json
end
