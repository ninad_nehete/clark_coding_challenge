# frozen_string_literal: true

module Exceptions
  class ApplicationError < StandardError; end

  class TreeInsertionError < ApplicationError; end

  class NodeNotFoundError < TreeInsertionError; end
end
