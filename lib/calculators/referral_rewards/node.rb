# frozen_string_literal: true

module Calculators
  module ReferralRewards
    # Node
    class Node
      attr_reader :value
      attr_accessor :options, :points, :parent, :state

      STATES = { invited: :invited,
                 accepted: :accepted }.freeze

      def initialize(value = nil, parent = nil, options = {})
        @value = value
        @options = options
        @parent = parent
        @points = 0
        @state = STATES[:invited]
      end

      def accepts?
        options[:accepts]
      end
    end
  end
end
