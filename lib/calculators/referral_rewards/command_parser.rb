# frozen_string_literal: true

module Calculators
  module ReferralRewards
    # CommandParser parses a string of command into object
    class CommandParser
      attr_reader :sequence_format

      def initialize(command_row, options = {})
        @command_row = command_row
        @sequence_format = options[:format]
      end

      def to_h
        hash = {}
        args.each_with_index { |arg, index| hash[sequence_format[index]] = arg }
        hash
      end

      private

      def args
        squish(@command_row).split(' ')
      end

      def squish(str)
        str.to_s.squeeze(' ').strip
      end
    end
  end
end
