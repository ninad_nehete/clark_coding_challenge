# frozen_string_literal: true

module Calculators
  module ReferralRewards
    # TreeBuilder builds a tree based on the commands
    class TreeBuilder
      attr_reader :commands

      def initialize(commands)
        @commands = commands
      end

      def execute(options = {})
        build_tree(options)
      end

      private

      def tree
        @tree ||= Tree.new
      end

      def build_tree(options = {})
        valid_commands(options).each do |command|
          if command[:action] == 'accepts'
            tree.update_reward_options(command[:user1], accepts: true)
          else
            tree.add_reward_node(command[:user1], command[:user2], from: command[:user1])
          end
        end
        tree
      end

      def valid_commands(options = {})
        validator = CommandValidator.new(commands, options)
        return validator.valid_commands if validator.validate

        raise Exceptions::TreeInsertionError, validator.errors
      end
    end
  end
end
