# frozen_string_literal: true

require 'dry/validation'
module Calculators
  module ReferralRewards
    # CommandContract contract for a valid command
    class CommandContract < Dry::Validation::Contract
      ALLOWED_ACTIONS = {
        recommends: 'recommends',
        accepts: 'accepts'
      }.freeze

      params do
        required(:date).filled(:date)
        required(:time).filled(:time)
        required(:user1).filled(:string)
        required(:action).filled(:string)
        optional(:user2).filled(:string)
      end

      rule(:action) do
        key.failure('unknown action') if ALLOWED_ACTIONS[value.to_sym].nil?
      end

      rule(:action, :user2) do
        key.failure('needs another user') if values[:action] == ALLOWED_ACTIONS[:recommends] && values[:user2].nil?
      end

      rule(:user1, :user2) do
        key.failure('cannot be same as another user') if values[:user1] == values[:user2]
      end

      rule(:action, :user2) do
        key.failure('needs only one user') if values[:action] == ALLOWED_ACTIONS[:accepts] && !values[:user2].nil?
      end
    end
  end
end
