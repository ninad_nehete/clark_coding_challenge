# frozen_string_literal: true

module Calculators
  module ReferralRewards
    # CommandValidator validates commands from a file
    class CommandValidator
      attr_reader :commands, :errors, :valid_commands, :options

      def initialize(commands, options = nil)
        @commands = commands
        @options = options
        @errors = []
        @valid_commands = []
      end

      def validate
        commands.each_line do |line|
          command = parser.new(line, options).to_h
          response = contract.new.call(command)
          if response.success?
            valid_commands << command
          else
            add_errors(line, response.errors)
          end
        end
        errors.empty?
      end

      private

      def contract
        options[:contract]
      end

      def parser
        options[:parser]
      end

      def add_errors(line, error)
        errors << { command: line, error: error.to_h }
      end
    end
  end
end
