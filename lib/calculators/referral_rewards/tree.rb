# frozen_string_literal: true

require_relative 'node'
require_relative '../../exceptions/base'
require_relative '../../../config/constants'

module Calculators
  module ReferralRewards
    # Tree to store nodes and their associations
    class Tree
      attr_accessor :root_node, :node_map

      MFACTOR = CONFIG['referral_bonus']['multiplication_factor']
      BASEPOINT = CONFIG['referral_bonus']['base_point']

      def initialize
        @root_node = Node.new(nil)
        @node_map = {}
      end

      def add_reward_node(parent, child, options = {})
        parent_node = node_map[parent]
        parent_node ||= insert(root_node, parent)
        child_node = node_map[child]
        insert(parent_node, child, options.merge(from: parent)) unless child_node
      end

      def update_reward_options(key, options)
        node = find(key)
        raise Exceptions::NodeNotFoundError if node.nil?

        node.options.merge!(options)
        update_parent_points(node, BASEPOINT) if update_parent_points?(node)
        node.state = :accepted
        node
      end

      def find(node)
        node_map[node]
      end

      def points
        points = {}
        node_map.each { |key, node| points[key] = node.points }
        points
      end

      private

      def update_parent_points?(node)
        # TODO: remove node data dependency
        node.accepts? && node.state != :accepted
      end

      def update_parent_points(node, points)
        return unless node.accepts?

        node.parent.points = node.parent.points + points
        update_parent_points(node.parent, points * MFACTOR)
      end

      def insert(node, value, options = {})
        return node if node.value == value

        new_node = Node.new(value, node, options)
        node_map[value] = new_node
        update_parent_points(new_node, BASEPOINT) if new_node.accepts?
        new_node
      end
    end
  end
end
