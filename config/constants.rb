# frozen_string_literal: true

CONFIG = YAML.safe_load(File.read('lib/data/calculation_constants.yaml'))
