# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Calculators::ReferralRewards::Tree do
  describe '#initialize' do
    it 'returns tree object' do
      tree = described_class.new
      expect(tree).not_to be_nil
      expect(tree.node_map.keys).to eq []
    end
  end

  describe '.insert' do
    it 'inserts a node to tree' do
      tree = described_class.new
      node = tree.add_reward_node('A', nil)
      expect(node.parent.value).to eq 'A'
    end

    it 'inserts a child node to a parent node tree' do
      tree = described_class.new
      node = tree.add_reward_node('A', 'B')
      expect(node.value).to eq 'B'
    end
  end

  describe '.update_parent_points' do
    context 'accepted node' do
      it 'adds points to first parent node' do
        tree = described_class.new
        tree.add_reward_node('A', 'B', accepts: true)
        expect(tree.points['A']).to eq 1
        expect(tree.points['B']).to eq 0
      end

      it 'adds a point to a grand parent node' do
        tree = described_class.new
        tree.add_reward_node('A', 'B', accepts: true)
        tree.add_reward_node('B', 'C', accepts: true)
        expect(tree.points['A']).to eq 1.5
      end

      it 'add points to a parent nodes only once for multiple nodes of same value' do
        tree = described_class.new
        tree.add_reward_node('A', nil, accepts: true)
        tree.add_reward_node('A', nil, accepts: true)
        expect(tree.points['A']).to eq 1
      end
    end

    context 'non accepted node' do
      it 'does not add points to a parent nodes' do
        tree = described_class.new
        tree.add_reward_node('A', 'B')
        expect(tree.points['A']).to eq 0
        expect(tree.points['B']).to eq 0
      end

      it 'does not add points to a new node that is not accepted' do
        tree = described_class.new
        tree.add_reward_node('A', 'B')
        tree.add_reward_node('A', nil, accepts: true)
        tree.add_reward_node('B', 'C')

        expect(tree.points['A']).to eq 1
        expect(tree.points['B']).to eq 0
        expect(tree.points['C']).to eq 0
      end

      xit 'should raise an error if it gets an accepted child node' do
        tree = described_class.new
        tree.add_reward_node('A', nil)
        tree.add_reward_node('A', 'B')
        tree.add_reward_node('B', 'C', accepts: true)

        expect(tree.points['A']).to eq 0
        expect(tree.points['B']).to eq 0
        expect(tree.points['C']).to eq 0
      end
    end
  end
end
