# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Calculators::ReferralRewards::TreeBuilder do
  describe '.calculate' do
    let(:config) do
      {
        parser: Calculators::ReferralRewards::CommandParser,
        contract: Calculators::ReferralRewards::CommandContract,
        format: %i[date time user1 action user2]
      }
    end

    it 'calculates rewards for commands ' do
      commands = "2018-06-12 09:41 A recommends B
                  2018-06-14 09:41 B accepts
                  2018-06-16 09:41 B recommends C
                  2018-06-17 09:41 C accepts
                  2018-06-19 09:41 C recommends D
                  2018-06-23 09:41 B recommends D
                  2018-06-25 09:41 D accepts"

      tree = described_class.new(commands).execute(config)
      expect(tree.points).to eq('A' => 1.75, 'B' => 1.5, 'C' => 1, 'D' => 0)
    end
  end
end
