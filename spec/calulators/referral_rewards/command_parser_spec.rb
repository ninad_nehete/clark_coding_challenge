# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Calculators::ReferralRewards::CommandParser do
  let(:command) { '2018-06-12 09:41 A recommends B' }
  let(:format) { %i[date time user1 action user2] }

  describe '#to_h' do
    context 'when command statement is passed' do
      it 'returns a hash' do
        subject = described_class.new(command, { format: format })
        expect(subject.to_h).to be_instance_of(Hash)
      end

      it 'parses parameters' do
        subject = described_class.new(command, { format: format })
        params = subject.to_h
        expect(params[:date]).to eq('2018-06-12')
        expect(params[:time]).to eq('09:41')
        expect(params[:user1]).to eq('A')
        expect(params[:action]).to eq('recommends')
        expect(params[:user2]).to eq('B')
      end

      it 'handles spaces and new lines in commands' do
        command = '   2018-06-12    09:41   A   recommends B  \n'
        subject = described_class.new(command, { format: format })
        params = subject.to_h

        expect(params[:date]).to eq('2018-06-12')
        expect(params[:time]).to eq('09:41')
        expect(params[:user1]).to eq('A')
        expect(params[:action]).to eq('recommends')
        expect(params[:user2]).to eq('B')
      end
    end
  end
end
