# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Calculators::ReferralRewards::CommandValidator do
  let(:command) { '2018-06-12 09:41 A recommends B' }
  let(:config) do
    {
      parser: Calculators::ReferralRewards::CommandParser,
      contract: Calculators::ReferralRewards::CommandContract,
      format: %i[date time user1 action user2]
    }
  end

  subject { described_class.new(command, config) }

  describe '#validate' do
    context 'when a valid command statement is passed' do
      it 'returns true' do
        expect(subject.validate).to eq(true)
        expect(subject.errors).to eq([])
      end
    end

    context 'when multiline valid command statements are passed' do
      let(:command) do
        '2018-06-12 09:41 A recommends B \n
                       2018-06-12 09:41 B recommends C'
      end
      it 'returns true' do
        expect(subject.validate).to eq(true)
        expect(subject.errors).to eq([])
      end
    end

    context 'when date format is incorrect' do
      let(:command) { '2018-06-1200 09:41 A recommends B' }

      it 'returns false' do
        expect(subject.validate).to eq(false)
        expect(subject.errors).not_to be_empty
      end
    end

    context 'when time format is incorrect' do
      let(:command) { '2018-06-12 009:410 A recommends B' }

      it 'returns false' do
        expect(subject.validate).to eq(false)
        expect(subject.errors).not_to be_empty
      end
    end

    context 'when action is not known' do
      let(:command) { '2018-06-12 09:41 A unkown_action B' }

      it 'returns false' do
        expect(subject.validate).to eq(false)
        expect(subject.errors).not_to be_empty
      end
    end

    context "second parameter is not passed for 'recommends' action" do
      let(:command) { '2018-06-12 09:41 A recommends ' }

      it 'returns true' do
        expect(subject.validate).to eq(false)
        expect(subject.errors).not_to be_empty
      end
    end

    context "second parameter is passed for 'recommends' action is same as first" do
      let(:command) { '2018-06-12 09:41 A recommends A' }

      it 'returns true' do
        expect(subject.validate).to eq(false)
        expect(subject.errors).not_to be_empty
      end
    end

    context "second parameter is passed for 'accepts' action" do
      let(:command) { '2018-06-12 09:41 A accepts B' }

      it 'returns true' do
        expect(subject.validate).to eq(false)
        expect(subject.errors).not_to be_empty
      end
    end
  end
end
