# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Calculators::ReferralRewards::Node do
  describe '#initialize' do
    it 'returns node with value' do
      node = described_class.new('USER')
      expect(node.value).to eq 'USER'
    end

    it 'returns the node with 0 as default points' do
      node = described_class.new
      expect(node.points).to eq 0
    end
  end
end
