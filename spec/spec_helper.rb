# frozen_string_literal: true

require 'simplecov'
SimpleCov.start
require 'rspec'
require 'require_all'
require_all 'lib'
