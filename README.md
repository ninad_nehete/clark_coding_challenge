# Referral Rewards Calculator

### Solution Brief:
##### Algorithm
The implementation is based on *tree* data structure. The stores the onboarded users in the *node*. The *user node* stores the link to the parent node which has recommended it. The points of the parent nodes are updated in a recursive fashion when this recommended node accepts the invitation.

This solution has been implemented using simple Sinatra instead of Rails to keep it as light weight as possible because
only one endpoint was need, no views and database was needed. For the validation purpose, Dry-Validation gem has been used which allows to build contracts which I believe are easy to build and maintain.

### Requirements:
 - `Ruby 3.0.0`

### Setup:
 - `bundle install`

### Starting Server:
 - `ruby main.rb`

### Testing endpoint with a file
 - `curl --location --request POST 'http://127.0.0.1:4567/calculate/referral_rewards' --form 'file=@"lib/data/sample/referral_commands.txt"'`
### Running Rubocop:
  - `bundle exec rubocop`

### Run Tests:
- `bundle exec rspec`


### Test Coverage Report:
Run tests then to view the generated test coverage report of specs visit `coverage/index.html`
